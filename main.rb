require './core/session'
require './core/handler'
require './core/httpserver'
require './app/app'

handler = Vsweb::CoreHandler.new
server = Vsweb::HttpServer.new(handler)
server.start()

loop {
  puts "Type 'q' to quit\n"
  STDOUT.flush
  input = gets
  if input == "q\n"
    break
  elsif input == "r\n"
    server.stop
    server.join
    server = Vsweb::HttpServer.new(nil)
    server.start
  end
}
