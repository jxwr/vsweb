require './core/action'
require './core/action_group'
require './core/publish'

class IndexAction < Vsweb::Action
  @@form_type_mapping = {
    "data" => :integer,
    "age" => :integer,
    "fruit" => :array_comma
  }

  def execute(person)
    return 'index', false
  end
end


