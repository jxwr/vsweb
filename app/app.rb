require './app/actions/index'

group = Vsweb::ActionGroup.new
group.add_action 'index', IndexAction
group.add_rhfile 'success.rh', 'success' # both url and rhfile can be end with ".rh" or not
group.add_rhfile 'error', 'error'        # vsweb will handle ext for us

Vsweb::publish_action_group "/", group
Vsweb::publish_dir "/data", "/data"
Vsweb::publish_dir "/images", "/images"
Vsweb::publish_dir "/", "/"
Vsweb::publish_dir "/views", "/views"
Vsweb::publish_file "/base/index.html", "/sample.html"
