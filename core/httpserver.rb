module Vsweb
  require 'gserver'

  ServerVersion = "0.1"
  CRLF = "\r\n"
  HTTP_PROTO = "HTTP/1.1"

  class StatusCode

    StatusCodeMapping = {
      200 => "OK",
      201 => "Created",
      202 => "Accepted",
      203 => "Non-Authoritative Information",
      204 => "No Content",
      205 => "Reset Content",
      206 => "Partial Cotent",

      300 => "Multiple Choices",
      301 => "Moved Permanently",
      302 => "Found",
      303 => "See Other",
      304 => "Not Modified",
      305 => "Use Proxy",
      307 => "Temporary Redirect",

      400 => "Bad Request",
      401 => "Unauthorized",
      402 => "Payment Required",
      403 => "Forbidden",
      404 => "Not Found",
      405 => "Method Not Allowed",
      406 => "Not Acceptable",
      407 => "Proxy Authenticaion Required",
      408 => "Request Timeout",
      409 => "Conflict",
      410 => "Gone",
      411 => "Length Required",
      412 => "Precondition Failed",
      413 => "Request Entity Too Large",
      414 => "Request-URI Too Long",
      415 => "Unsupported Media Type",

      500 => "Internal Server Error",
      501 => "Not Implemented",
      502 => "Bad Gateway",
      503 => "Service Unavailable",
      504 => "Gateway Timeout",
      505 => "HTTP Version Not Supported"
    }

    def self.render(code)
      "<html><head><title>#{code} #{StatusCodeMapping[code]}</title></head>" + 
        "<body><h1><center>#{code}: #{StatusCodeMapping[code]}</center></h1><hr/>" + 
        "<center>vs #{ServerVersion}</center></body></html>"
    end
    
    def self.to_description(code)
      StatusCodeMapping[code]
    end
  end

  class HttpRequest
    attr_reader :method, :path, :proto, :header, :params
    
    def initialize(method, path, proto, header, params)
      @method, @path, @proto, @header, @params = method, path, proto, header, params
    end
  end

  class HttpResponse
    attr_accessor :status, :header, :body, :is_static_file

    def initialize(status=200)
      @status = status
      @header = {}
      @body = ""
      @is_static_file = false
    end

    def static_file?
      @is_static_file
    end

    def push_error(code)
      @status = code
      @body = StatusCode.render(code)
    end

    def resp_head()
      out = ""
      out << "#{HTTP_PROTO} #{@status} #{StatusCode.to_description(@status)}" << CRLF
      @header.each { |k, v| out << "#{k}: #{v}" << CRLF }
      out << CRLF
      out
    end

    def write(io)
      out = resp_head()
      out << @body unless @body.nil?

      io << out
    end

    def write_file(io)
      io << resp_head()
      IO.copy_stream(@body, io)
    end
  end

  class HttpServer < GServer
    def initialize(handler, port = 8080, host = DEFAULT_HOST, max_connections = 4, stdlog = $stdout, audit = true, debug = false)
      @handler = handler
      logfile = File.open("log.txt", "w")
      super(port, host, max_connections, logfile, audit, debug)
    end

    def parse_params(src)
      params = {}
      pairs = src.split('&')
      pairs.each do |pair|
        if not pair.index('=').nil?
          kv = pair.split('=')
          params[kv[0]] = kv[1]
        end
      end
      params
    end

    def serve(io)
      response = HttpResponse.new

      if io.gets =~ /^(\S+)\s+(\S+)\s+(\S+)/
        method, path, proto = $1, $2, $3
      else
        response.push_error(400)
        response.write(io)
        return
      end

      header = {}
      while (line = io.gets) !~ /^(\n|\r)/
        # puts"[request]:%s" % line
        header[$1] = $2.strip if line =~ /^([\w-]+):\s*(.*)$/
      end

      params = {}
      if method == "GET" or method == "HEAD"
        path, args = *path.split('?')
        params = parse_params(args) unless args.nil?
      else
        unless len = header["Content-Length"].nil?
          body = io.read(header["Content-Length"].to_i)
          params = parse_params(body)
        end
      end

      io.binmode
      request = HttpRequest.new(method, path, proto, header, params)
      @handler.process(request, response)

      # puts "[body]:%s" % body
      # puts "[params]:%s" % params
      # puts "[path]:%s" % path
      # puts "\n[response]: %s" % response.body

      if response.static_file?
        response.write_file(io)
      else
        response.write(io)
      end
    end
  end

end
