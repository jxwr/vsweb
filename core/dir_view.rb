module Vsweb

  class DirView
    def self.render(dir)
      edir = "./app" + dir
      puts edir

      title = "<title>%s</title>" % edir
      content = "<h1>Index of %s</h1><hr/>" % dir

      
      content << "<table width='100%'>"
      content << "<tr><th>Name</th><th>Size</th><th>Date</th></tr>"


      Dir.glob(edir+"*") do |entry|
        basename = File.basename(entry)
        content << "<tr>"
        content << "<td><a href=\"#{dir+basename}\">#{basename}</a></td>" 
        content << "<td><center>%s</center></td>" % File.size(entry)
        content << "<td><center>%s</center></td>" % File.ctime(entry)
        content << "</tr>"
      end

      content << "</table>"

      body = "<body>%s<hr/><center>Vsweb #{ServerVersion}</center></body>" % content

      html = "<html><head>%s<head>%s</html>" % [title, body]
    end
  end

end # Vsweb
