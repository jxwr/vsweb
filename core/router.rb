module Vsweb
  
  class Router
    ActionGroupMapping = {}
    FileMapping = {}
    DirMapping = {}

    ROUTE_ERROR = [:route_error, "route error"]

    def route(path)
      # if path end with '/', it is either an action_group with a
      # default action or a dir, otherwise it's an action or a file
      puts "path:" + path
      if path.end_with? '/'
        if action_group = ActionGroupMapping[path]
          if action_group.default_action
            return [:action, action_group.default_action] 
          elsif action_group.default_rhfile
            return [:rhfile, action_group.default_rhfile] 
          else
            return ROUTE_ERROR
          end
        elsif dir = DirMapping[path]
          return [:dir, dir]
        end
      else
        idx = path.rindex('/')
        dir = path[0..idx]
        entry = path[idx+1..path.size-1]

        action_group = ActionGroupMapping[dir]

        if entry.end_with?(".rh") and action_group and action_group.rhfiles[entry]
          return [:rhfile, action_group.rhfiles[entry]]
        elsif action_group and action_group[entry] and action_group[entry].superclass == Action
          puts "route:"
          return [:action, action_group[entry]]
        elsif file = FileMapping[path]
          file = "./app" + file
          if File.exists?(file) and not File.directory?(file) 
            puts file
            return [:file, file] 
          end
        elsif fdir = DirMapping[dir]
          file = "./app" + fdir + entry
          if File.exists?(file) and not File.directory?(file) 
            return [:file, file] 
          end
        end
      end
      return ROUTE_ERROR
    end
  end # Router

end # Vs
