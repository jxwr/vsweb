module Vsweb

  class Action
    @@form_type_mapping = {}

    def form_type_mapping()
      @@form_type_mapping
    end

    def execute(form)
    end
  end # Action

end # Vs
