module Vsweb

  class Invoker
    @@interceptor_stack = []

    def self.add_interceptor(interceptor)
      @@interceptor_stack << interceptor.new
      puts "+interceptor: %s" % interceptor
    end

    def initialize(action_context)
      @action_context = action_context
      @cursor = 0
    end

    def invoke()
      if @cursor < @@interceptor_stack.size
        @cursor += 1
        @@interceptor_stack[@cursor-1].intercept self
      else 
        puts @action_context.action
        @action_context.action.execute @action_context.model
      end
    end

    # this must be written after method defined, 
    # since Interceptor would use class method ::add_interceptor
    Dir.glob('./core/interceptor/*.rb').sort.each do |entry|
      require entry
    end
  end # Invoker

end # Vsweb
