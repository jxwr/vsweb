module Vsweb
  
  class ActionGroup
    attr_accessor :default_action, :default_rhfile, :rhfiles
    
    def initialize()
      @actions = {}
      @rhfiles = {}
      @default_action = nil
      @default_rhfile = nil
    end

    def [](name)
      @actions[name]
    end

    def add_action(name, action)
      @actions[name] = action
      @default_action = action if @default_action.nil?
    end

    def add_rhfile(name, rhfile)
      rhfile = rhfile[0..-4] if rhfile.end_with? ".rh"
      name = name + ".rh" unless name.end_with? ".rh"

      @rhfiles[name] = rhfile
      @default_rhfile = rhfile if @default_rhfile.nil?
    end
  end

end # Vs
