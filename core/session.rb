module Vsweb

  ##
  # 1. What does session do?
  #   Connect a series of requests.
  # 2. When does session begin?
  #   Server wants to.
  # 3. When does session end?
  #   Expire.
  # 4. Where does session data store?
  #   Server side or cookie.
  class Session
    DEFAULT_EXPIRY = 30
    attr_reader :session_id
    attr_accessor :session_expiry, :session_data

    def restore
      return if @session_id.nil?

      puts "restore"

      return unless File.exist?("./app/session/" + @session_id)
      
      file = File.new("./app/session/" + @session_id)
      data = Marshal.load(file)
      file.close

      puts data

      @session_create_time = data[:session_create_time] || Time.now
      @session_expiry = data[:session_expiry] || DEFAULT_EXPIRY
      @session_data = data[:session_data] || {}

      if @session_create_time + @session_expiry < Time.now
        puts "session expiry"
        need_new_cookie!
        @session_expiry = DEFAULT_EXPIRY
        @session_data = {}
      end
    end

    def store
      puts "store"

      return if @session_id.nil?

      data = {
        :session_create_time => @session_create_time,
        :session_expiry => @session_expiry,
        :session_data => @session_data
      }

      p data

      File.open("./app/session/" + @session_id, 'w') do |file|
        Marshal.dump(data, file)
      end
    end

    def initialize(request)
      @need_new_cookie = false
      @session_data = {}
      @session_expiry = DEFAULT_EXPIRY
      @session_create_time = Time.now

      cookie = request.header['Cookie']

      if cookie.nil?
        @session_id = nil
      else
        @session_id = /session_id=(?<id>.*)(;|\t|$)/.match(cookie)[:id]
        puts @session_id
        restore unless @session_id.nil?
      end
    end

    def [](key)
      @session_data[key]
    end

    def []=(key, value)
      if @session_id.nil? and @session_data.empty?
        need_new_cookie!
        @session_id = gen_session_id
        @session_data = {}
      end

      @session_data[key] = value

      puts @session_data
    end

    def need_new_cookie?
      @need_new_cookie
    end

    def need_new_cookie!
      @need_new_cookie = true
      @session_id = gen_session_id
      @session_create_time = Time.now
    end

    def gen_session_id
      values = 
        [rand(0x0010000),
         rand(0x0010000),
         rand(0x0010000),
         rand(0x0010000),
         rand(0x0010000),
         rand(0x1000000),
         rand(0x1000000)
        ]

      "%04x%04x%04x%04x%04x%06x%06x" % values
    end

    def to_cookie
      "session_id=" + @session_id
    end
  end

end # Vsweb
