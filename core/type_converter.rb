module Vsweb
  
  class TypeConverter

    def self.url_decode(v)
      v.gsub(/%(..)/) { "0x#$1".to_i(16).chr }
    end

    def self.cast(v, type)
      v = url_decode(v)

      return v if type.nil?

      puts v

      case type
      when :integer
        v.to_i
      when :float
        v.to_f
      when :array
        v.split
      when :array_comma
        v.split ','
      else
        ""
      end
    end
  end

end # Vsweb
