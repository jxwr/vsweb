module Vsweb
  require './core/router'
  require './core/invoker'
  require './core/action_context'
  require './core/dir_view'
  require './core/view'
  
  class CoreHandler
    
    def initialize()
      @router = Router.new
    end

    def session_store(action_context, response)
        if action_context.session.need_new_cookie?
          puts "set cookie %s" % action_context.session.to_cookie
          response.header['Set-Cookie'] = action_context.session.to_cookie
        end

        action_context.session.store
    end

    ##
    # route url to action and invoke
    def process(request, response)
      type, obj = *@router.route(request.path)

      response.status = 200
      response.is_static_file = false
      
      puts "[%s]\n" % request.method
      puts "=route===================\n"

      case type
      when :action
        puts "->action"
        action_context = ActionContext.new(request, obj)
        invoker = Invoker.new(action_context)
        puts "-model:%s" % action_context.model.inspect

        puts "\n-invoke--------------"
        result, redirect = invoker.invoke()
        puts "-invoke-end----------\n"

        puts "\n-render----------------"
        response.body = View.render(result, action_context)
        puts "-render-end------------\n"

        session_store(action_context, response)

#        response.status = 303
#        response.header['Location'] = "http://localhost:8080/images/ddd.bmp"
      when :rhfile
        puts "rhfile:%s" % obj
        action_context = ActionContext.new(request, Action)
        puts "-model:%s" % action_context.model.inspect

        response.body = View.render(obj, action_context)

        session_store(action_context, response)
      when :file
        puts "file:%s" % obj
        response.is_static_file = true
        response.body = obj
      when :dir
        puts "dir:%s" % obj
        response.body = DirView.render(obj)
      when :route_error
        puts "route error"
        response.push_error(404)
      else
        response.push_error(500)
      end # case

      puts "=========================\n\n"

      rescue
        response.push_error(500)
    end # process
  end # CoreHandler

end # Vsweb
