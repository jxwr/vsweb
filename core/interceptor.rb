module Vsweb
  require './core/invoker'

  class Interceptor
    def self.push(inter)
      Invoker.add_interceptor(inter)
    end
  end # Interceptor

end # Vsweb
