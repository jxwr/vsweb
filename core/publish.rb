module Vsweb
  require './core/router'

  def self.publish_file(url_pattern, file_path)
    Router::FileMapping[url_pattern] = file_path
  end

  # url_pattern must end with '/' as well as dir_path
  def self.publish_dir(url_pattern, dir_path)
    url_pattern << '/' unless url_pattern.end_with? '/'
    dir_path << '/' unless dir_path.end_with? '/'
    Router::DirMapping[url_pattern] = dir_path
  end

  def self.publish_action_group(url_pattern, action_group)
    Router::ActionGroupMapping[url_pattern] = action_group
  end

end
