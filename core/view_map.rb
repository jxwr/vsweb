module Vsweb
  class ViewMap
    @@map = {}

    HEAD_STR = <<STRHEAD
require './core/view_map'

class AutoGen_%s
  def self.<<(context)
    _M = context.model
    _SESSION = context.session
    out = ""
STRHEAD

    TAIL_STR = <<STRTAIL
    out
  rescue
    puts "rh render failed"
  end
end

Vsweb::ViewMap[\'%s\'] = AutoGen_%s
STRTAIL

    def self.[]=(key, view)
      @@map[key] = view
    end

    def self.[](key)
      view = @@map[key]

      if view.nil?
        rbfile = "./app/views/tmp/#{key}.rb"
        rhfile = "./app/views/#{key}.rh"

        print "[%s]:" % key

        if File.exists? rbfile
          require rbfile
        elsif File.exists? rhfile
          translate rhfile
          require rbfile
        else
          return nil
        end
        
        view = @@map[key]
      end
      
      view
    end

    def self.translate(rhfile)
      name = File.basename(rhfile, ".rh")

      body_str = IO.read(rhfile)
      body_str = "$>" + body_str + "<$"

      body_str.gsub!(/<\$=(.*)\$>/, '<$    out += \1 $>')
      body_str.gsub!(/\$>/, "\n    out += <<STREE\n")
      body_str.gsub!(/<\$/, "\nSTREE\n")
      
      File.open("./app/views/tmp/" + name + ".rb", "w") do |file|
        file.puts HEAD_STR % name
        file.puts body_str
        file.puts TAIL_STR % [name, name]
      end
  end
  
    def self.translate_all()
      Dir.glob("./app/views/*.rh") do |entry|
        puts "+rhfile:" + entry
        self.translate entry
      end
    end
  end

end # Vsweb

