module Vsweb
  require './core/view_map'

  class View
    ViewMap.translate_all
    
    def self.render(result, context)
      model = context.model

      print "-view:" 
      view = ViewMap[result]
      puts view
      view << context
    end
  end # View

end # Vsweb
