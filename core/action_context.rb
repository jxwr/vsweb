module Vsweb

  class ValueModel
    def set(name, value)
      self.class.class_eval { attr_accessor name.to_sym }
      instance_variable_set(('@'+name).to_sym, value)
    end
  end # ValueModel

  require './core/type_converter'
  require './core/session'

  class ActionContext
    attr_reader :action, :request
    attr_accessor :model, :session
    
    def initialize(request, action)

      @request = request
      @action = action.new
      @model = ValueModel.new
      @session = Session.new(request)

      request.params.each do |k, v|
        puts "context"      
        type = @action.form_type_mapping[k]
        v = TypeConverter.cast(v, type)
        @model.set(k, v)
      end
    end
  end # ActionContext

end # Vsweb
