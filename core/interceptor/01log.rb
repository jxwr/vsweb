module Vsweb
  require './core/interceptor'
  
  class LogInterceptor < Interceptor
    push self

    def intercept(invoker)
      puts "log begin"
      result = invoker.invoke
      puts "log end"

      result
    end
  end # LogInterceptor

end # Vsweb
