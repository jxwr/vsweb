module Vsweb
  require './core/interceptor'
  
  class ParamsInterceptor < Interceptor
    push self

    def intercept(invoker)
      puts "params begin"
      result = invoker.invoke
      puts "params end"

      result
    end
  end # ParamsInterceptor

end # Vsweb
